class CreateMappings < ActiveRecord::Migration[5.1]
  def change
    create_table :mappings do |t|
      t.string :code
      t.string :url
      t.datetime :expires_at
      t.integer :times_used, default: 0

      t.timestamps
    end
    add_index :mappings, :code,                unique: true
    
  end
end
