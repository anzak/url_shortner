Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users

  resource :mappings, only: [:new, :create]
  root 'mappings#new'
  # get 'new' => 'mappings#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/:code' => 'mappings#show'
end
