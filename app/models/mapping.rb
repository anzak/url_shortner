class Mapping < ApplicationRecord
  validates :url, :code, presence: true

  def self.get_correct_mapping params
    mapping = Mapping.new(params)
    mapping.code = random_generator if mapping.code.blank?
    mapping
  end

  # This counld be many things:
  # base 64 of the url
  # completely random
  # sequential... guessable?
  # hash of url... not really useful? but less chance of collision
  # or this? ...  1 in 26*26*10*8 chance
  def self.random_generator len:8
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    (0...len).map { o[rand(o.length)] }.join
  end
end
