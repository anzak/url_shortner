class MappingsController < ApplicationController
  def show
    mapping = Mapping.find_by_code(params[:code])
    if mapping && mapping.url.present?
      mapping.update_attributes times_used: mapping.times_used+1
      redirect_to mapping.url
    end
  end

  def new
    @mapping = Mapping.new
  end

  def create
    @mapping = Mapping.get_correct_mapping(mapping_params)
    @mapping.save!
    flash[:notice] = "You can now go to #{@mapping.url} by entering localhost:3000/#{@mapping.code}"
    redirect_to root_path
  rescue ActiveRecord::RecordNotUnique
    flash[:error] = "The code already exists, please select something else"
    render :new
  rescue ActiveRecord::ActiveRecordError
    flash[:error] = "Your mapping could not be created"
    render :new
  end

  def mapping_params
    params.require(:mapping).permit(:url, :code)
  end

end
